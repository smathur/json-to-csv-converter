import pandas as pd
import os

# find all .cdx.json files and use pandas to individually convert them from json to csv

files = []
for root, dirs, filenames in os.walk('.'):
    for f in filenames:
        if f.endswith('.cdx.json'):
            files.append(os.path.join(root, f))

for f in files:
    df = pd.read_json(f)
    df.to_csv(f.replace('.cdx.json', '.csv'), index=False)