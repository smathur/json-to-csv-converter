# JSON to CSV Converter

## Setup

To run dependency scanning with CSV outputs, simply add the following to `.gitlab-ci.yml` (if Dependency Scanning isn't already added to the pipeline, this YAML config will include it):

```yaml
include:
  - remote: 'https://gitlab.com/smathur/json-to-csv-converter/raw/main/csv_dependency_scanning.gitlab-ci.yml'
```

All generated CSV reports will appear as job artifacts, which can be browsed by clicking into a pipeline > clicking into the `cdx_json_to_csv` job, and clicking "Browse".

## What it does

1. Including the above YAML config adds GitLab's [Dependency Scanning](https://gitlab.com/gitlab-org/gitlab/-/raw/master/lib/gitlab/ci/templates/Jobs/Dependency-Scanning.gitlab-ci.yml) template and a `cdx_json_to_csv` job to the pipeline. The `cdx_json_to_csv` job runs in a separate stage called "convert_cdx" after the "test" stage, where Dependency Scanning runs.
2. The `cdx_json_to_csv` job runs a [Python script](https://gitlab.com/smathur/json-to-csv-converter/-/blob/main/json_to_csv.py) that takes all CycloneDX JSON files and converts them into CSV using Pandas 1.5.2.